<?php

namespace App\Repository;

use App\Entity\PresentList;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method PresentList|null find($id, $lockMode = null, $lockVersion = null)
 * @method PresentList|null findOneBy(array $criteria, array $orderBy = null)
 * @method PresentList[]    findAll()
 * @method PresentList[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PresentListRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PresentList::class);
    }

    // /**
    //  * @return PresentList[] Returns an array of PresentList objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?PresentList
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
