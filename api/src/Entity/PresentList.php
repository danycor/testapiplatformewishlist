<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\PresentListRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass=PresentListRepository::class)
 */
class PresentList
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToMany(targetEntity=Present::class, inversedBy="presentListes")
     */
    private $presents;

    /**
     * @ORM\ManyToOne(targetEntity=People::class, inversedBy="presentListCreator")
     * @ORM\JoinColumn(nullable=false)
     */
    private $creator;

    /**
     * @ORM\ManyToMany(targetEntity=People::class, inversedBy="presentLists")
     */
    private $contributors;

    /**
     * @ORM\ManyToOne(targetEntity=People::class, inversedBy="presentListsTarget")
     */
    private $target;

    public function __construct()
    {
        $this->presents = new ArrayCollection();
        $this->contributors = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Collection|Present[]
     */
    public function getPresents(): Collection
    {
        return $this->presents;
    }

    public function addPresent(Present $present): self
    {
        if (!$this->presents->contains($present)) {
            $this->presents[] = $present;
        }

        return $this;
    }

    public function removePresent(Present $present): self
    {
        $this->presents->removeElement($present);

        return $this;
    }

    public function getCreator(): ?People
    {
        return $this->creator;
    }

    public function setCreator(?People $creator): self
    {
        $this->creator = $creator;

        return $this;
    }

    /**
     * @return Collection|People[]
     */
    public function getContributors(): Collection
    {
        return $this->contributors;
    }

    public function addContributor(People $contributor): self
    {
        if (!$this->contributors->contains($contributor)) {
            $this->contributors[] = $contributor;
        }

        return $this;
    }

    public function removeContributor(People $contributor): self
    {
        $this->contributors->removeElement($contributor);

        return $this;
    }

    public function getTarget(): ?People
    {
        return $this->target;
    }

    public function setTarget(?People $target): self
    {
        $this->target = $target;

        return $this;
    }
}
