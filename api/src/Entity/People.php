<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\PeopleRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass=PeopleRepository::class)
 */
class People
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $city;

    /**
     * @ORM\OneToMany(targetEntity=PresentList::class, mappedBy="creator", orphanRemoval=true)
     */
    private $presentLists;

    /**
     * @ORM\OneToMany(targetEntity=PresentList::class, mappedBy="target")
     */
    private $presentListsTarget;

    /**
     * @ORM\OneToMany(targetEntity=PresentList::class, mappedBy="creator")
     */
    private $presentListCreator;

    public function __construct()
    {
        $this->presentLists = new ArrayCollection();
        $this->presentListsTarget = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(string $city): self
    {
        $this->city = $city;

        return $this;
    }

    /**
     * @return Collection|PresentList[]
     */
    public function getPresentLists(): Collection
    {
        return $this->presentLists;
    }

    public function addPresentList(PresentList $presentList): self
    {
        if (!$this->presentLists->contains($presentList)) {
            $this->presentLists[] = $presentList;
            $presentList->setCreator($this);
        }

        return $this;
    }

    public function removePresentList(PresentList $presentList): self
    {
        if ($this->presentLists->removeElement($presentList)) {
            // set the owning side to null (unless already changed)
            if ($presentList->getCreator() === $this) {
                $presentList->setCreator(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|PresentList[]
     */
    public function getPresentListsTarget(): Collection
    {
        return $this->presentListsTarget;
    }

    public function addPresentListsTarget(PresentList $presentListsTarget): self
    {
        if (!$this->presentListsTarget->contains($presentListsTarget)) {
            $this->presentListsTarget[] = $presentListsTarget;
            $presentListsTarget->setTarget($this);
        }

        return $this;
    }

    public function removePresentListsTarget(PresentList $presentListsTarget): self
    {
        if ($this->presentListsTarget->removeElement($presentListsTarget)) {
            // set the owning side to null (unless already changed)
            if ($presentListsTarget->getTarget() === $this) {
                $presentListsTarget->setTarget(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|PresentList[]
     */
    public function getPresentListsCreator(): Collection
    {
        return $this->presentListCreator;
    }

    public function addPresentListsCreator(PresentList $presentListCreator): self
    {
        if (!$this->presentListCreator->contains($presentListCreator)) {
            $this->presentListCreator[] = $presentListCreator;
            $presentListCreator->setCreator($this);
        }
        return $this;
    }

    public function removePresentListCreator(PresentList $presentListCreator): self
    {
        if ($this->presentListCreator->removeElement($presentListCreator)) {
            // set the owning side to null (unless already changed)
            if ($presentListCreator->getCreator() === $this) {
                $presentListCreator->setCreator(null);
            }
        }

        return $this;
    }
}
