<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\PresentRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass=PresentRepository::class)
 */
class Present
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=0)
     */
    private $price;

    /**
     * @ORM\Column(type="string", length=1024, nullable=true)
     */
    private $image;

    /**
     * @ORM\ManyToMany(targetEntity=PresentList::class, mappedBy="presents")
     */
    private $presentListes;

    public function __construct()
    {
        $this->presentListes = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getPrice(): ?string
    {
        return $this->price;
    }

    public function setPrice(string $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(?string $image): self
    {
        $this->image = $image;

        return $this;
    }

    /**
     * @return Collection|PresentList[]
     */
    public function getPresentListes(): Collection
    {
        return $this->presentListes;
    }

    public function addPresentListes(PresentList $presentListes): self
    {
        if (!$this->presentListes->contains($presentListes)) {
            $this->presentListes[] = $presentListes;
            $presentListes->addPresent($this);
        }

        return $this;
    }

    public function removePresentListes(PresentList $presentListes): self
    {
        if ($this->presentListes->removeElement($presentListes)) {
            $presentListes->removePresent($this);
        }

        return $this;
    }
}
