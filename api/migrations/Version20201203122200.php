<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201203122200 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP SEQUENCE greeting_id_seq CASCADE');
        $this->addSql('CREATE SEQUENCE people_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE present_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE present_list_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE "user_id_seq" INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE people (id INT NOT NULL, name VARCHAR(255) NOT NULL, city VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE present (id INT NOT NULL, name VARCHAR(255) NOT NULL, price NUMERIC(10, 0) NOT NULL, image VARCHAR(1024) DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE present_list (id INT NOT NULL, creator_id INT NOT NULL, target_id INT DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_BCF352361220EA6 ON present_list (creator_id)');
        $this->addSql('CREATE INDEX IDX_BCF3523158E0B66 ON present_list (target_id)');
        $this->addSql('CREATE TABLE present_list_present (present_list_id INT NOT NULL, present_id INT NOT NULL, PRIMARY KEY(present_list_id, present_id))');
        $this->addSql('CREATE INDEX IDX_93383D5AF5A02015 ON present_list_present (present_list_id)');
        $this->addSql('CREATE INDEX IDX_93383D5A8D7B1EF8 ON present_list_present (present_id)');
        $this->addSql('CREATE TABLE present_list_people (present_list_id INT NOT NULL, people_id INT NOT NULL, PRIMARY KEY(present_list_id, people_id))');
        $this->addSql('CREATE INDEX IDX_C783C0B1F5A02015 ON present_list_people (present_list_id)');
        $this->addSql('CREATE INDEX IDX_C783C0B13147C936 ON present_list_people (people_id)');
        $this->addSql('CREATE TABLE "user" (id INT NOT NULL, people_id INT NOT NULL, email VARCHAR(180) NOT NULL, roles JSON NOT NULL, password VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8D93D649E7927C74 ON "user" (email)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8D93D6493147C936 ON "user" (people_id)');
        $this->addSql('ALTER TABLE present_list ADD CONSTRAINT FK_BCF352361220EA6 FOREIGN KEY (creator_id) REFERENCES people (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE present_list ADD CONSTRAINT FK_BCF3523158E0B66 FOREIGN KEY (target_id) REFERENCES people (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE present_list_present ADD CONSTRAINT FK_93383D5AF5A02015 FOREIGN KEY (present_list_id) REFERENCES present_list (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE present_list_present ADD CONSTRAINT FK_93383D5A8D7B1EF8 FOREIGN KEY (present_id) REFERENCES present (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE present_list_people ADD CONSTRAINT FK_C783C0B1F5A02015 FOREIGN KEY (present_list_id) REFERENCES present_list (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE present_list_people ADD CONSTRAINT FK_C783C0B13147C936 FOREIGN KEY (people_id) REFERENCES people (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE "user" ADD CONSTRAINT FK_8D93D6493147C936 FOREIGN KEY (people_id) REFERENCES people (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('DROP TABLE greeting');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE present_list DROP CONSTRAINT FK_BCF352361220EA6');
        $this->addSql('ALTER TABLE present_list DROP CONSTRAINT FK_BCF3523158E0B66');
        $this->addSql('ALTER TABLE present_list_people DROP CONSTRAINT FK_C783C0B13147C936');
        $this->addSql('ALTER TABLE "user" DROP CONSTRAINT FK_8D93D6493147C936');
        $this->addSql('ALTER TABLE present_list_present DROP CONSTRAINT FK_93383D5A8D7B1EF8');
        $this->addSql('ALTER TABLE present_list_present DROP CONSTRAINT FK_93383D5AF5A02015');
        $this->addSql('ALTER TABLE present_list_people DROP CONSTRAINT FK_C783C0B1F5A02015');
        $this->addSql('DROP SEQUENCE people_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE present_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE present_list_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE "user_id_seq" CASCADE');
        $this->addSql('CREATE SEQUENCE greeting_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE greeting (id INT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('DROP TABLE people');
        $this->addSql('DROP TABLE present');
        $this->addSql('DROP TABLE present_list');
        $this->addSql('DROP TABLE present_list_present');
        $this->addSql('DROP TABLE present_list_people');
        $this->addSql('DROP TABLE "user"');
    }
}
